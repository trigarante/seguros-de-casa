module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
           link: [
          { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,700' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */

  build: {
     vendor: ['jquery', 'bootstrap'],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  css: ['bootstrap/dist/css/bootstrap.css','static/css/styles.css'],
  modules: [
  
  ['@nuxtjs/google-tag-manager', { id: 'GTM-KRCFZQ' }],
  ['@nuxtjs/font-awesome'],
  ['nuxt-validate', {
      lang: 'es'
  }]
  ],
  env: {
    urlDB: 'https://ahorraseguros.mx/ws-rest/servicios',
    //urlDB: 'http://192.168.10.65:8080/ws-rest/servicios'
    //NODE_ENV: 'production'
  },
  render: {
    http2:{ push: true },
    resourceHints:false,
    gzip:{ threshold: 9 }
  },
  router: {
      base: '/seguro-de-casa/'
  }
}

