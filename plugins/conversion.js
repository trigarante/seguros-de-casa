const conversion ={}

conversion.gtag_report_conversion=function(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
    'send_to': 'AW-860099105/_Ev9CJPc-G4QoaSQmgM',
    'event_callback': callback
  });
  return false;
}
export default conversion
